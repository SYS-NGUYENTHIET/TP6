#include <stdlib.h>
#include <stdio.h>

int *allouer_tableau(int dimension, int val);

int *lire_n_entiers(int n);

void liberer_tableau(int *tab);

void afficher_tableau(int *tab, int dimension);

int *lire_entiers(void);

void tab_copy_n(int *t1, int *t2, int n);

int *init_tab(int n);

void afficher_tableau_finit_zero(int *t);

int **allouer_matrice(int lignes, int colonnes, int val);

void liberer_matrice(int **mat, int ligne);

void afficher_matrice(int **mat, int lignes, int colonnes);