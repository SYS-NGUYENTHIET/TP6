#include "exo2.h"

int *initialiser_tableau_v1(int n, int valeur)
{
    int len = 1;
    int *tab = (int *)malloc(sizeof(int) * len);
    tab[0] = valeur;

    while (2 * len <= n)
    {
        tab = (int *)realloc(tab, sizeof(int) * len * 2);
        memcpy(tab + len, tab, sizeof(int) * len);

        len *= 2;
    }

    tab = (int *)realloc(tab, sizeof(int) * len * 2);
    memcpy(tab + len, tab, sizeof(int) * (n - len));

    return tab;
}

char *initialiser_tableau_char(int dimension, char c)
{
    char *res = (char *)malloc(sizeof(char) * dimension);
    return (char *)memset((void *)res, (int)c, dimension);
}

/**
 * On utilise memmove car la taille + 1er index peut chevaucher avec le 2e index
 */
void copier_chaine(char *tab, int s1, int taille, int s2)
{
    memmove(tab + s2,tab + s1, taille);
}

void print_tab_int(int *tab, int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("%d-", tab[i]);
    }
}

void print_tab_char(char *tab, int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("%c-", tab[i]);
    }
}

int main(void)
{
    int size = 20;
    int *p = initialiser_tableau_v1(size, 5);
    print_tab_int(p, size);
    printf("\n");

    char *t = initialiser_tableau_char(size, 'a');
    print_tab_char(t, size);
    printf("\n");

    char s[10] = "abcdefghij";
    printf("%s", s);
    printf("\n");

    copier_chaine(s, 0, 4, 6);
    printf("%s",s);

    return 0;
}