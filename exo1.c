#include "exo1.h"

int *allouer_tableau(int dimension, int val)
{
    int *res = (int *)malloc(sizeof(int) * dimension);

    for (int i = 0; i < dimension; i++)
    {
        res[i] = val;
    }

    return res;
}

int *lire_n_entiers(int n)
{
    int *res = (int *)malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", res + i);
    }
    return res;
}

void liberer_tableau(int *tab)
{
    free(tab);
}

void afficher_tableau(int *tab, int dimension)
{
    for (int i = 0; i < dimension; i++)
    {
        printf("%d-", tab[i]);
    }
}

int *lire_entiers(void)
{
    int capacity = 4;
    int size = 0;
    int number;
    int *res = init_tab(4);
    // int *tmp;

    do
    {
        scanf("%d", &number);
        res[size] = number;
        size++;
        if (size >= capacity)
        {
            capacity = capacity * 2;
            res = realloc(res,capacity);
        }
    } while (number != 0);

    return res;
}

void tab_copy_n(int *t1, int *t2, int n)
{
    for (int i = 0; i < n; i++)
    {
        t2[i] = t1[i];
    }
}

int *init_tab(int n)
{
    return (int *)malloc(sizeof(int) * n);
}

void afficher_tableau_finit_zero(int *t)
{
    int i = 0;
    while (t[i] != 0)
    {
        printf("%d-", t[i]);
        i++;
    }
}

int **allouer_matrice(int lignes, int colonnes, int val)
{
    int **res = (int **)malloc(sizeof(int *) * lignes);
    for (int i = 0; i < lignes; i++)
    {
        res[i] = (int *)malloc(sizeof(int) * colonnes);
        for (int j = 0; j < colonnes; j++)
        {
            res[i][j] = val;
        }
    }

    return res;
}

void liberer_matrice(int **mat, int lignes)
{
    for (int i = 0; i < lignes; i++)
    {
        free(mat[i]);
    }
    free(mat);
}

void afficher_matrice(int **mat, int lignes, int colonnes)
{
    for (int i = 0; i < lignes; i++)
    {
        afficher_tableau(mat[i], colonnes);
        printf("\n");
    }
}

int main()
{
    int size = 10;
    int *t1 = allouer_tableau(size, 5);
    afficher_tableau(t1, size);
    liberer_tableau(t1);
    printf("\n");

    int *t2 = lire_n_entiers(size);
    afficher_tableau(t2, 10);
    liberer_tableau(t2);
    printf("\n");

    int *t3 = lire_entiers();
    afficher_tableau_finit_zero(t3);
    liberer_tableau(t3);
    printf("\n");

    int **m = allouer_matrice(5, 5, 10);
    afficher_matrice(m, 5, 5);
    liberer_matrice(m, 5);

    return 0;
}